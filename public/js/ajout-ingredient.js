window.onload = function(){
    const formulaire = document.querySelector("#button")

    formulaire.addEventListener("click", () => {
        const nomIngredient = document.getElementById("nom-ingredient").value 
        
        if(nomIngredient == ""){
            alert("Veuillez remplir le champs : nom de l'ingrédient")
            return
        }
        if(nomIngredient == "null"){
            alert("Veuillez mettre un nom correst à l'ingrédient");
            return
        }

        const parametre = {
            "typeRequete" : 1,
            "nom" : nomIngredient
        }
        let requete = new XMLHttpRequest()
        requete.open('POST', '/ajout-ingredient')
        requete.setRequestHeader('content-type', 'application/json')
        requete.onload = function(){
            if(requete.responseText == "success"){
                alert(`L'ajout de votre ingrédient : "${nomIngredient}" a bien été effectué`)
            }else{
                alert(`Une erreur est survenue lors de l'ajout de votre ingrédient' !`)
            }
        }
        requete.send(JSON.stringify(parametre))
    })
}