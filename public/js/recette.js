function recupIngredient(){
    const parametres = {
        "typeRequete" : 1
    }
    let requete = new XMLHttpRequest()
    requete.open('POST', '/creation-recette')
    requete.setRequestHeader('content-type', 'application/json')
    requete.onload = function(){
        const ingredients = JSON.parse(requete.responseText)
        const listIngredients = document.querySelector('#ingredients')

        Object.keys(ingredients).forEach((key, value) => {
            let ingredient = document.createElement("div")
            ingredient.setAttribute("id", `ingredient${key}`)
            ingredient.setAttribute("class", 'ingredients')
            listIngredients.appendChild(ingredient)

            let labelIngredient = document.createElement("label")
            labelIngredient.setAttribute("id", `label-ingredient${key}`)
            labelIngredient.setAttribute("name", `${key}`)
            labelIngredient.innerHTML = `${ingredients[key]["nom"]}`
            ingredient.appendChild(labelIngredient)

            let inputIngredient = document.createElement("input")
            inputIngredient.setAttribute("type", "checkbox")
            inputIngredient.setAttribute("name", `${key}`)
            inputIngredient.setAttribute("id", `checkbox-ingredient${key}`)
            inputIngredient.setAttribute("class", "checkbox-ingredients")
            inputIngredient.setAttribute("value", `${key}`)
            ingredient.appendChild(inputIngredient)


        })
    }
    requete.send(JSON.stringify(parametres))
}

window.onload = function (){
    recupIngredient()
    const formulaire = document.querySelector("#button")

    formulaire.addEventListener("click", () => {
        const nomRecette = document.getElementById('nom-recette').value
        const nomCreateur = document.getElementById('nom-createur').value
        if(nomRecette == '' || nomCreateur == ''){
            alert("Veuillez remplir tous les champs !")
            return
        }
        const divListIngredients = document.querySelector("#ingredients")
        let tableauIngredients = []
        
        for(let i = 1; i <= divListIngredients.parentNode.children.length; i++){
            let ingrediente = document.querySelector("#checkbox-ingredient"+i)
            if(ingrediente.checked == true){
                tableauIngredients.push(i)
            }
        }

        if(tableauIngredients.length == 0){
            alert("Veuillez choisir au moins un ingrédient")
            return
        }

        const parametres = {
            "typeRequete" : 2,
            "nom" : nomRecette,
            "ingredients" : tableauIngredients,
            "createur" : nomCreateur
        }
        let requete = new XMLHttpRequest()
        requete.open('POST', '/creation-recette')
        requete.setRequestHeader('content-type', 'application/json')
        requete.onload = function(){
            if(requete.responseText == "success"){
                alert(`Votre recette "${nomRecette}" a bien été créée`)
            }else{
                alert(`Une erreur est survenue lors de la création de votre recette !`)
            }
        }
        requete.send(JSON.stringify(parametres))
    })
    
    
}


