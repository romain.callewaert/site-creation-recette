const http = require('http');
const express = require('express');
const fs = require('fs'); //File system

const app = express();
const PORT_HTTP = 8282;

const fichier = fs.readFileSync(__dirname+'/BDD.json');
const BDD = JSON.parse(fichier);

// console.log(Object.keys(BDD.recettes).length)



app.use(express.static(__dirname+'/public'));
app.use('/css', express.static(__dirname+'public/css'));
app.use('/js', express.static(__dirname+'public/js'));
app.use('/images', express.static(__dirname+'public/images'));
app.use(express.json());

app.set('view engine', 'ejs');

app.get('/', function(req, res){
    res.render(__dirname+'/views/index');
});
app.get('/creation-recette', function(req, res){
    res.render(__dirname+'/views/creation-recette');
});
app.get('/ajout-ingredient', function(req, res){
    res.render(__dirname+'/views/ajout-ingredient');
});
app.get('/voir-recette', function(req, res){
    res.render(__dirname+'/views/voir-recette');
});
app.get('*', function(req, res){
    res.render(__dirname+'/views/404');
});

app.post('/creation-recette', (req,res)=>{
    if(req.body.typeRequete == 1){
        //Récupération des ingrédients
        res.send(BDD["ingredients"]);
    }else if(req.body.typeRequete == 2){
        //Création d'une recette
        const countRecettes = Object.keys(BDD.recettes).length;

        const recette = {
            "id": `${countRecettes+1}`,
            "nom": req.body.nom,
            "ingredients": req.body.ingredients,
            "createur": req.body.createur
        }

        BDD.recettes[countRecettes+1] = recette;

        // console.log(BDD.recettes);

        fs.writeFileSync(__dirname+'/BDD.json', JSON.stringify(BDD));

        res.send("success");
    }else{
        //Erreur
        res.send("Requete invalide");
    }
});

app.post('/ajout-ingredient', (req,res)=>{
    if(req.body.typeRequete == 1){
        //Ajout d'un ingrédient
        const countIngredients = Object.keys(BDD.ingredients).length;
        const ingredient = {
            "id": `${countIngredients+1}`,
            "nom": req.body.nom,
        }

        BDD.ingredients[countIngredients+1] = ingredient;

        // console.log(BDD.ingredients);

        fs.writeFileSync(__dirname+'/BDD.json', JSON.stringify(BDD));

        res.send("success");
    }else{
        res.send("Requete invalide");
    }
    
});

app.listen(PORT_HTTP, (req, res)=>{
    console.log(`Serveur démarré sur http://localhost:${PORT_HTTP}`);
});